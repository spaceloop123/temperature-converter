

exports.cToF = function (c) {
    const num = parseFloat(c);

    return ((num * 9 / 5) + 32).toFixed(2);
}

exports.fToC = function (f) {
    const num = parseFloat(f);

    return ((num - 32) * 5 / 9).toFixed(2);
}
